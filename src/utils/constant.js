import {
  BookmarkAltIcon,
  CalendarIcon,
  ChartBarIcon,
  CursorClickIcon,
  MenuIcon,
  PhoneIcon,
  PlayIcon,
  RefreshIcon,
  ShieldCheckIcon,
  SupportIcon,
  ViewGridIcon,
  XIcon,
  OfficeBuildingIcon,
  UsersIcon,
  TerminalIcon,
  ServerIcon,
  BadgeCheckIcon,
  DatabaseIcon,
  DesktopComputerIcon,
  DeviceMobileIcon,
  LinkIcon,
  ExternalLinkIcon,
  CloudIcon,
} from '@heroicons/vue/outline'
export const HEADERS = [
  { multiple: false, name: "Home", href: "/" },
  { multiple: false, name: "About us", href: "#about-us" },
  { multiple: false, name: "Services", href: "#services" },
  { multiple: false, name: "Projects", href: "#projects" },
  {
    multiple: true, name: "Solutions", href: "#solutions", children: [
      {
        name: "Security",
        description: "Your customers' data will be safe and secure.",
        href: "security",
        icon: ShieldCheckIcon,
      },
      {
        name: "Integrations",
        description: "Connect with third-party tools that you're already using.",
        href: "integrations",
        icon: ViewGridIcon,
      },
      {
        name: "Automations",
        description:
          "Build strategic funnels that will drive your customers to convert",
        href: "automations",
        icon: RefreshIcon,
      },

    ]
  },
  { multiple: false, name: "Contact", href: "/contact" }
];
// import img from "~/assets/images/project/sirfunding1.png"
import logoSirfunding from "~/assets/images/project/logo-sirfunding.png"
import sirfunding1 from "~/assets/images/project/sirfunding1.png"
import sirfunding2 from "~/assets/images/project/sirfunding2.png"
import sirfunding3 from "~/assets/images/project/sirfunding3.png"
import sirfunding4 from "~/assets/images/project/sirfunding4.png" 
import logoAs22 from "~/assets/images/project/logo-as22.png"
import as21 from "~/assets/images/project/as21.png"
import as22 from "~/assets/images/project/as22.png"
import as23 from "~/assets/images/project/as23.png"
import as24 from "~/assets/images/project/as24.png"
import as25 from "~/assets/images/project/as25.png" 
import translator1 from "~/assets/images/project/translator1.png"
import translator2 from "~/assets/images/project/translator2.png"
import translator3 from "~/assets/images/project/translator3.png"
import portal1 from "~/assets/images/project/portal1.png"
import portal2 from "~/assets/images/project/portal2.png"
import portal3 from "~/assets/images/project/portal3.png"
import portal4 from "~/assets/images/project/portal4.png"
import a150 from "~/assets/images/project/150.png"

export const PROJECTS = [
  {
    name: "Sirfunding",
    short_description: "SirFunding® is a pioneer Digital/Technological platform in implementing the blockchain system with a proposal for investment and logistics in international trade. Innovating in financial processes through the Fintech model, creating a cutting-edge solution in global marketing.",
    description:
      "Sirfunding is a trading and investment marketplace implemented with blockchain technologies for international trade. Greicodex raises the processes and defines the technological platform for the development of the project using Web3 technologies and Jamstack architecture, designed to make the web application faster, more secure and easier to scale.",
    icon: DesktopComputerIcon,
    img: logoSirfunding,
    logo: logoSirfunding,
    link: "sirfunding",
    technologies: [
      { name: "React (Frontend)", key: 1 },
      { name: "Nodejs (Backend)", key: 2 },
      { name: "Ethereum APIs", key: 4 },
      { name: "IPFS", key: 5 },
    ],
    images: [
      { key: 1, src: sirfunding1, },
      { key: 2, src: sirfunding2, },
      { key: 3, src: sirfunding3, },
      { key: 4, src: sirfunding4, }
    ]
  },
  {
    name: "OpenAS2",
    short_description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium doloribus reprehenderit deleniti tempore tempora, excepturi eius laboriosam molestias pariatur,",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas voluptatum rerum dolore, repellendus illum delectus explicabo officia nulla totam facere deserunt nostrum vitae quo sequi soluta expedita sunt perspiciatis eveniet?",
    icon: DeviceMobileIcon,
    img: logoAs22,
    logo: logoAs22,
    link: "openas2",
    technologies: [
      { name: "Vuejs (Frontend)", key: 1 },
      { name: "Java (Backend)", key: 2 }
    ],
    images: [
      { key: 1, src: as21, },
      { key: 2, src: as22, },
      { key: 3, src: as23, },
      { key: 4, src: as24, },
      { key: 5, src: as25, }
    ],
  },
  {
    name: "Translator",
    short_description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium doloribus reprehenderit deleniti tempore tempora, excepturi eius laboriosam molestias pariatur,",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas voluptatum rerum dolore, repellendus illum delectus explicabo officia nulla totam facere deserunt nostrum vitae quo sequi soluta expedita sunt perspiciatis eveniet?",
    icon: LinkIcon,
    img: translator1,
    logo: translator1,
    link: "translator",
    technologies: [
      { name: "(Frontend)", key: 1 },
      { name: " (Mysql)", key: 2 }
    ],
    images: [
      { key: 1, src: translator1, },
      { key: 2, src: translator2, },
      { key: 3, src: translator3, },
    ],
  },
  {
    name: "Employee Portal",
    short_description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium doloribus reprehenderit deleniti tempore tempora, excepturi eius laboriosam molestias pariatur,",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas voluptatum rerum dolore, repellendus illum delectus explicabo officia nulla totam facere deserunt nostrum vitae quo sequi soluta expedita sunt perspiciatis eveniet?",
    icon: OfficeBuildingIcon,
    img: portal1,
    logo: portal1,
    link: "employee-portal",
    technologies: [
      { name: "Vuejs (Frontend)", key: 1 },
      { name: "Laravel (Backend)", key: 2 },
      { name: "Mysql (Database)", key: 3 }

    ],
    images: [
      { key: 1, src: portal1, },
      { key: 2, src: portal2, },
      { key: 3, src: portal3, },
      { key: 4, src: portal4, },
    ],
  },
  {
    name: "OCR/IA/ML",
    short_description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium doloribus reprehenderit deleniti tempore tempora, excepturi eius laboriosam molestias pariatur,",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas voluptatum rerum dolore, repellendus illum delectus explicabo officia nulla totam facere deserunt nostrum vitae quo sequi soluta expedita sunt perspiciatis eveniet?",
    icon: DatabaseIcon,
    img: a150,
    logo: a150,
    link: "orc-ia-ml",
    technologies: [
      { name: "Vuejs", key: 1 },
      { name: "Mysql", key: 2 }
    ],
    images: []
  },
  {
    name: "Speech/ Voice / Telco ",
    short_description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium doloribus reprehenderit deleniti tempore tempora, excepturi eius laboriosam molestias pariatur,",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas voluptatum rerum dolore, repellendus illum delectus explicabo officia nulla totam facere deserunt nostrum vitae quo sequi soluta expedita sunt perspiciatis eveniet?",
    icon: BadgeCheckIcon,
    img: a150,
    logo: a150,
    link: "speech-voice-telco",
    technologies: [
      { name: "Vuejs", key: 1 },
      { name: "Mysql", key: 2 }
    ],
    images: []
  },
  {
    name: "bfor",
    short_description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium doloribus reprehenderit deleniti tempore tempora, excepturi eius laboriosam molestias pariatur,",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas voluptatum rerum dolore, repellendus illum delectus explicabo officia nulla totam facere deserunt nostrum vitae quo sequi soluta expedita sunt perspiciatis eveniet?",
    icon: BadgeCheckIcon,
    img: a150,
    logo: a150,
    link: "bfor",
    technologies: [
      { name: "Flutter (Android,iOS)", key: 1 },
    ],
    images: []
  },
];

export const IMAGES_PROJECT = {
  "sirfunding": [
    { key: 1, src: "assets/images/project/sirfunding1.png", },
    { key: 2, src: "assets/images/project/sirfunding2.png", },
    { key: 3, src: "assets/images/project/sirfunding3.png", },
    { key: 4, src: "assets/images/project/sirfunding4.png", }
  ],
  "openAS2": [
    { key: 1, src: "assets/images/project/as21.png", },
    { key: 2, src: "assets/images/project/as22.png", },
    { key: 3, src: "assets/images/project/as23.png", },
    { key: 4, src: "assets/images/project/as24.png", },
    { key: 5, src: "assets/images/project/as25.png", }
  ],
  "translator": [
    { key: 1, src: "assets/images/project/translator1.png", },
    { key: 2, src: "assets/images/project/translator2.png", },
    { key: 3, src: "assets/images/project/translator3.png", },
  ],
  "employeePortal": [
    { key: 1, src: "assets/images/project/portal1.png", },
    { key: 2, src: "assets/images/project/portal2.png", },
    { key: 3, src: "assets/images/project/portal3.png", },
    { key: 4, src: "assets/images/project/portal4.png", },
  ],
  "OCRIAML": [],
  "SpeechVoiceTelco": []
};

export const SERVICES = [
  {
    name: "Web Development",
    bgColor:"bg-black",
    title:[{color:"text-gray-100", text:"WEB APPLICATION"},{color:"text-indigo-600", text:"DEVELOPMENT"}],
    description:
      "We build high-performance, secure and scalable web applications, which are tailor-made to customer requirements.",
    large_description: "We can build your website or develop your e-commerce application to integrate with your existing web site or e-commerce system. Our team of experts and industry consultants will work with you to understand your needs and objectives, and recommend the best technology solutions. We have the knowledge, experience, and resources to deliver quality solutions, and ensure your success.",
    icon: DesktopComputerIcon,
    link: "web-development",
    img: "assets/images/mohammad-rahmani-gA396xahf-Q-unsplash@2x.png"
  },
  {
    name: "Mobile Development",
    bgColor:"bg-gray-300",
    title:[{color:"text-gray-900", text:"MOBILE APPLICATION"},{color:"text-indigo-600", text:"DEVELOPMENT"}],
    description:
      "Globally, mobile technology has emerged as a primary engine of economic growth. We can help you build responsive and user-friendly mobile applications for Android and iOS.",
    large_description: "Greicodex Software Mobile Developers are mold breakers. Analytical with an expansive knowledge base in UX and UI trends, our team creates strategies that go beyond the normal scope of mobile apps, creating unique experiences while elevating your project and business to the next level. In an ever growing and competitive market, the best way to stand out from the crowd is to think outside the box, and shatter expectations!",
    icon: DeviceMobileIcon,
    link: "mobile-development",
    img: "assets/images/greicodex-mobile-development-892769.jpg"
  },
  {
    name: "Blockchain Development",
    bgColor:"bg-gray-900",
    title:[{color:"text-gray-100", text:"BLOCKCHAIN"},{color:"text-indigo-600", text:"DEVELOPMENT"}],
    description:
      "Tap into the power of blockchains. We build high-performance, secure and scalable Web 3.0 applications, which are tailor-made to customer requirements. ",
    large_description:
      "Tap into the power of blockchains. We build high-performance, secure and scalable Web 3.0 applications, which are tailor-made to customer requirements. ",
    icon: LinkIcon,
    link: "blockchain-development",
    img: "assets/images/greicodex-cryptocurrencies-1097946.jpg"
  },
  {
    name: "Machine Learning Solutions",
    bgColor:"bg-indigo-900",
    title:[{color:"text-gray-100", text:"MACHINE"},{color:"text-indigo-300", text:"LEARNING"}],
    description:
      "We offer machine learning and AI solutions that can be used to solve various business problems.",
    large_description: "Machine learning is the ability of a computer to learn without being explicitly programmed. It is a form of artificial intelligence (AI) that allows computers to learn without being explicitly programmed. It is a subset of the broader field of artificial intelligence. Our specialists can create and train neural networks on Amazon Sage or Azure Machine Learning.",
    icon: OfficeBuildingIcon,
    link: "machine-learning",

    img: "assets/images/greicodex-machine-learning-2599244.jpg"
  },
  {
    name: "Data Integration",
    bgColor:"bg-gray-300",
    title:[{color:"text-gray-900", text:"DATA"},{color:"text-indigo-800", text:"INTEGRATION"}],
    description:
      "Data is the new oil and we have the right team of data scientists to help you turn your data into insights.",
    large_description: "Data integration is the process of combining data from different sources into a single, unified view. Integration begins with the ingestion process, and includes steps such as cleansing, ETL mapping, and transformation.",
    icon: DatabaseIcon,
    link: "data-integration",

    img: "assets/images/greicodex-data-integration-577210.jpg"
  },
  {
    name: "Project Rescuing",
    bgColor:"bg-gray-300",
    title:[{color:"text-gray-900", text:"PROJECT"},{color:"text-indigo-800", text:"RESCUING"}],
    description:
      "If your project is in trouble, our team of experts will help you get it back on track in no time.",
    large_description:
      "If your project is in trouble, our team of experts will help you get it back on track in no time.",
    icon: BadgeCheckIcon,
    link: "project-rescuing",
    img: "assets/images/greicodex-team-studio-3810792.jpg"
  },
  {
    name: "DevOps",
    bgColor:"bg-indigo-900",
    title:[{color:"text-gray-100", text:"DEVOPS"},{color:"text-indigo-200", text:"SYSTEM ADMIN"}],
    description:
      "We help you automate and streamline your software development and delivery process.",
    large_description: "Ultimately, the aim of a DevOps engineer is to make every section of an IT company collaborative in nature. On the other hand, a sysadmin is more narrowly focused on configuring, keeping up and maintaining servers and computer systems.",
    icon: ServerIcon,
    link: "devops",
    img: "assets/images/devops-3155972_1280.jpg"
  },
  {
    name: "Outsource Teams",
    bgColor:"bg-gray-300",
    title:[{color:"", text:"OUTSOURCE"},{color:"", text:"TEAMS"}],
    description:
      "Having your software team geographically close comes with many advantages. While we provide a significant cost advantage we share the same time zone, work culture and communication standards, allowing maximum productivity. ",
    large_description: "Outsourced development teams can quickly and efficiently take on new projects to keep up with demand. As IT workloads continue to grow, it becomes more and more difficult to manage and complete projects without outside help. Outsourcing is a cost-effective option for many companies and allows managers to focus their attention on the tasks that are the most pressing. While outsourcing is a great way to get projects done on time, companies still need to be vigilant in how they work with their outsourced partners.",
    icon: UsersIcon,
    link: "outsource-teams",
    img: "assets/images/greicodex-team-studio-3810792.jpg"
  },
  {
    name: "Enterprise Solutions",
    bgColor:"bg-gray-100",
    title:[{color:"text-gray-900", text:"ENTERPRISE"},{color:"text-indigo-800", text:"SOLUTIONS"}],
    description:
      "We develop enterprise solutions that are robust, scalable and secure, and help businesses automate their processes, improve their efficiency and increase their bottom line.",
    large_description: "We develop enterprise solutions that are robust, scalable and secure, and help businesses automate their processes, improve their efficiency and increase their bottom line.",
    icon: OfficeBuildingIcon,
    link: "enterprise-solutions",
    img: "assets/images/greicodex-enterprise-solutions-3912948.jpg"
  },
  {
    name: "Cloud Solutions",
    bgColor:"bg-indigo-200",
    title:[{color:"text-gray-900", text:"CLOUD"},{color:"text-indigo-800", text:"SOUTIONS"}],
    description:
      "We can help you migrate to the cloud and take advantage of the many benefits it has to offer.",
    large_description: "Cloud computing is all about sharing resources in order to achieve consistency and feasibilities of scale over a network. Cloud Computing Consulting Services are offered in in the market by many vendors but our impeccable solutions help us stand out in the crowd.",
    icon: CloudIcon,
    link: "cloud-solutions",
    img: "assets/images/greicodex-cloud-computing-109479.jpg"
  },
];


export const CONTACT = {
  address: "Centro Solano Plaza. Torre 1. Piso 9. Oficina PH-B. Av. Francisco Solano c/ Calle La Iglesia. Sabana Grande. Parroquia El Recreo. Municipio Libertador.",
  emails: [`ventas@greicodex.com`, `administracion@greicodex.com`],
  phones: [`Venezuela +58 212 750.2137 / 762.9120 / 761.8071`, `Usa +1 305 507.8391`],
}

export const SOCIAL_NETWORK = [
  {
    name: "Facebook",
    icon: "facebook-f",
    link: "https://www.facebook.com/greicodex",
    d: "M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z",
    classIcon: "w-4",
    viewBox: "0 0 512 512"

  },
  {
    name: "Twitter",
    icon: "twitter",
    link: "https://www.twitter.com/greicodex",
    d: "M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z",
    classIcon: "w-4",
    viewBox: "0 0 512 512"

  },
  // {
  //   name: "Google",
  //   icon: "google",
  //   link: "#",
  //   d: "M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z",
  //   classIcon: "w-4",
  //   viewBox: "0 0 488 512"

  // },
  {
    name: "Instagram",
    icon: "instagram",
    link: "https://www.instagram.com/greicodex",
    d: "M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z",
    classIcon: "w-4",
    viewBox: "0 0 448 512"

  },
  // {
  //   name: "Linkedin-in",
  //   icon: "linkedin-in",
  //   link: "https://www.linkedin.com/greicodex",
  //   d: "M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z",
  //   classIcon: "w-4",
  //   viewBox: "0 0 448 512"
  // },
  {
    name: "Github",
    icon: "github",
    link: "https://www.github.com/greicodex",
    d: "M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z",
    classIcon: "w-4",
    viewBox: "0 0 496 512"

  }];

// export const MENUS = [
//   {
//     name: 'Solutions',
//     href: '#',
//     submenus: [
//       { name: 'Security', description: "Your customers' data will be safe and secure.", href: '#', icon: "ShieldCheckIcon" },
//       {
//         name: 'Integrations',
//         description: "Connect with third-party tools that you're already using.",
//         href: '#',
//         icon: "ViewGridIcon",
//       },
//       {
//         name: 'Automations',
//         description: 'Build strategic funnels that will drive your customers to convert',
//         href: '#',
//         icon: "RefreshIcon",
//       },
//     ]
//   },
//   {
//     name: 'Pricing',
//     href: '#',
//     submenus: []
//   }, {
//     name: "Dosc",
//     href: "#",
//     submenus: []
//   },
//   {
//     name: "More",
//     href: "#",
//     submenus: [
//       { id: 1, description: "", name: 'Boost your conversion rate', href: '#', icon: "" },
//       { id: 2, description: "", name: 'How to use search engine optimization to drive traffic to your site', href: '#', icon: "" },
//       { id: 3, description: "", name: 'Improve your customer experience', href: '#', icon: "" },
//     ]
//   }
// ]

export const FEATURES = [
  {
    name: "We understand you",
    description:
      "We simplify the problem statement by dividing it into small digestible bits so you understand what we are doing and what the problem is. Our primary focus while solving a problem for any business is to solve it in a customer-centric and scalable approach.",
    icon: "01",
  },
  {
    name: "Digital products used by thousands",
    description:
      "We'll create a website that is not only engaging, interactive and fast but also sells. By discussion and analysis, we'll get to know its customers and purpose. We'll help you create content and propose an original and functional design that we'll program. A website is no good without visitors, so we'll advise you how to get them there and test it to make the most of it.",
    icon: "02",
  },
  {
    name: "Right in your market",
    description:
      "Our unique competence in various technologies enables us to deliver integrated, scalable, robust solutions with the optimal cost/performance ratio. We constantly conduct research on new technology products to meet the ever-growing needs of our customers",
    icon: "03",
  },
  {
    name: "More than a mere service provider",
    description:
      "Maybe you have a good idea and no time to explore your options. Let's sit down and see how we can help. We don't propose complicated solutions to earn money. We'll help you to find the most effective solution, so our service isn't a black hole for your funds.",
    icon: "04",
  },
];
