import { defineNuxtConfig } from 'nuxt'
import { viteSingleFile } from "vite-plugin-singlefile"
// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    target: "static",
    // app: {
    //     "baseURL": "/",
    //     "buildAssetsDir": "/_nuxt/",
    //     "cdnURL": ""
    //   },
    // components: {
    //     global: true,
    //     dirs: ['~/components']
    // },
    nitro: {
        prerender: {
            routes: ['/',
                '/contact',
                '/solution',
                "/project",
                "/service",
                '/project/sirfunding',
                '/project/openas2', 
                "/project/translator",
                "/project/employee-portal",
                "/project/orc-ia-ml",
                "/project/speech-voice-telco",
                "/project/bfor",
                "/service/web-development",
                "/service/mobile-development",
                "/service/blockchain-development",
                "/service/machine-learning",
                "/service/data-integration",
                "/service/project-rescuing",
                "/service/devops",
                "/service/outsource-teams",
                "/service/enterprise-solutions",
                "/service/cloud-solutions",
                "/solution/security",
                "/solution/integrations",
                "/solution/automations"
            ]
        }
    },
    ssr: false,
    runtimeConfig: {
        // app:{
        //     buildAssetsDir:"http://127.0.0.1:3000"
        // },
        // API_SECRET: 'passw0rd!',
        public: {
            API_BASE_URL: 'http://127.0.0.1:3000'
        }
    },
    head: {
        title: 'Greicodex Software - Nearshoring Software Development',
        meta: [
            // <meta name="viewport" content="width=device-width, initial-scale=1">
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' },
        ],
        script: [
            // <script src="https://myawesome-lib.js"></script>
            // { src: 'https://awesome-lib.js' }
        ],
        link: [
            // <link rel="stylesheet" href="https://myawesome-lib.css">
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://awesome-lib.css' }
        ],
        style: [
            // <style type="text/css">:root { color: red }</style>
            //  { children: ':root { color: red }', type: 'text/css' }
            { children: "* { font-family: 'Nunito', sans-serif !important;}", type: 'text/css' }
        ]
    },
    buildModules: [
        '@nuxtjs/tailwindcss',
        '@tailwindcss/forms',
    ],

    router: {
        base: '/',
    },
    srcDir: 'src/',
    build: {
        transpile: ['@headlessui/vue', '@heroicons/vue'],
        extractCSS: true
    },
    performance: {
        gzip: true
    },
})
